package Test;

import java.util.Scanner;

public class Ylesanne2 {

    public static void main(String[] args) {
        System.out.println("Sisesta üks täisarv: ");
        Scanner readInput = new Scanner(System.in);
        String arv1 = readInput.nextLine();
        System.out.println("Sisesta veel üks täisarv: ");
        String arv2 = readInput.nextLine();

        Boolean x;
        x = arv1.equals(arv2);
        if(x) {
            System.out.println("Need arvud on võrdsed.");
        }
        else {
            System.out.println("Need arvud ei ole võrdsed.");
        }
        readInput.close();
    }
}